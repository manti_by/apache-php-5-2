#! /bin/sh

# php -r "phpinfo();" | grep -A 5 bz2
set -e
set -u

# # ZIP
# apt install zlib1g-dev

# BCMATH
php-ext-install bcmath

#BZ2
apt-get install -y libbz2-dev
pecl install bz2
echo "extension=bz2.so" > /etc/php/conf.d/ext-bz2.ini

# CALENDAR
php-ext-install calendar

# CURL
apt-get install -y libcurl4-openssl-dev
php-ext-install curl


# EXIF
php-ext-install exif

#FTP
php-ext-install ftp

# GD
apt-get install -y libjpeg-dev libpng-dev
php-ext-configure gd --with-jpeg-dir=/usr/ && php-ext-install gd

# GETTEXT
php-ext-install gettext

# ICONV
php-ext-install iconv

# IMAP
apt-get install -y libc-client-dev libkrb5-dev
php-ext-configure imap --with-kerberos --with-imap-ssl \
&& php-ext-install imap

# MCRYPT
apt-get install -y libmcrypt-dev
php-ext-install mcrypt

# mysql
apt-get install -y libmysqlclient-dev
php-ext-install mysqli
# openssl
cp /usr/src/php/ext/openssl/config0.m4 /usr/src/php/ext/openssl/config.m4
php-ext-install openssl

# pdo_mysql
php-ext-install pdo_mysql

# shmop
php-ext-install shmop

# sockets
php-ext-install sockets

# ssh2
apt-get install -y libssh2-1-dev
yes '' | pecl install ssh2-0.13
echo "extension=ssh2.so" > /etc/php/conf.d/ssh2.ini

# sysvmsg
php-ext-install sysvmsg

# tidy
apt-get install -y libtidy-dev
php-ext-install tidy

# WDDX
php-ext-install wddx

# XLS
apt-get install -y libxslt-dev
php-ext-install xsl

# ZIP
php-ext-install zip

#ZLIB
cp /usr/src/php/ext/zlib/config0.m4 /usr/src/php/ext/zlib/config.m4
php-ext-install zlib

#MB_String
php-ext-install mbstring

# MIME_MAGIC
php-ext-install mime_magic
